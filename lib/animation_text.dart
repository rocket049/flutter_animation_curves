import 'package:flutter/material.dart';

class MyAnimationText extends StatefulWidget {
  const MyAnimationText(
      {super.key, required this.text, required this.curveVal});
  final String text;
  final Curve curveVal;

  @override
  State<StatefulWidget> createState() {
    return _MyAnimationText();
  }
}

class _MyAnimationText extends State<MyAnimationText>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late CurvedAnimation _curve;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 2));
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        _curve = CurvedAnimation(parent: _controller, curve: widget.curveVal);
        Animation<Offset> pos =
            Tween<Offset>(begin: const Offset(0, 1), end: const Offset(0, 0))
                .animate(_curve);

        _controller.forward(from: 0);
        return SlideTransition(
          position: pos,
          child: Text(widget.text),
        );
      },
    );
  }
}
