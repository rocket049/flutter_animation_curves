import 'package:animation_curves/animation_text.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a blue toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Curve curve = Curves.linear;

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the other colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              '平移动画的各种 curve 效果:',
            ),
            Row(
              children: [
                DropdownButton<Curve>(
                  value: curve,
                  items: const [
                    DropdownMenuItem(
                        value: Curves.linear, child: Text('Curves.linear')),
                    DropdownMenuItem(
                        value: Curves.bounceIn, child: Text('Curves.bounceIn')),
                    DropdownMenuItem(
                        value: Curves.bounceInOut,
                        child: Text('Curves.bounceInOut')),
                    DropdownMenuItem(
                        value: Curves.bounceOut,
                        child: Text('Curves.bounceOut')),
                    DropdownMenuItem(
                        value: Curves.decelerate,
                        child: Text('Curves.decelerate')),
                    DropdownMenuItem(
                        value: Curves.ease, child: Text('Curves.ease')),
                    DropdownMenuItem(
                        value: Curves.easeIn, child: Text('Curves.easeIn')),
                    DropdownMenuItem(
                        value: Curves.easeInOut,
                        child: Text('Curves.easeInOut')),
                    DropdownMenuItem(
                        value: Curves.easeOut, child: Text('Curves.easeOut')),
                    DropdownMenuItem(
                        value: Curves.easeInBack,
                        child: Text('Curves.easeInBack')),
                    DropdownMenuItem(
                        value: Curves.easeInCirc,
                        child: Text('Curves.easeInCirc')),
                    DropdownMenuItem(
                        value: Curves.easeInCubic,
                        child: Text('Curves.easeInCubic')),
                    DropdownMenuItem(
                        value: Curves.easeInExpo,
                        child: Text('Curves.easeExpo')),
                    DropdownMenuItem(
                        value: Curves.easeInOutBack,
                        child: Text('Curves.easeInOutBack')),
                    DropdownMenuItem(
                        value: Curves.easeInOutCirc,
                        child: Text('Curves.easeInOutCirc')),
                    DropdownMenuItem(
                        value: Curves.easeInOutCubic,
                        child: Text('Curves.easeInOutCubic')),
                    DropdownMenuItem(
                        value: Curves.easeInOutCubicEmphasized,
                        child: Text('Curves.easeInOutCubicEmphasized')),
                    DropdownMenuItem(
                        value: Curves.easeInOutExpo,
                        child: Text('Curves.easeInOutExpo')),
                    DropdownMenuItem(
                        value: Curves.easeInOutQuad,
                        child: Text('Curves.easeInOutQuad')),
                    DropdownMenuItem(
                        value: Curves.easeInOutQuart,
                        child: Text('Curves.easeInOutQuart')),
                    DropdownMenuItem(
                        value: Curves.easeInOutQuint,
                        child: Text('Curves.easeInOutQuint')),
                    DropdownMenuItem(
                        value: Curves.easeInOutSine,
                        child: Text('Curves.easeInOutSine')),
                    DropdownMenuItem(
                        value: Curves.easeOutBack,
                        child: Text('Curves.easeOutBack')),
                    DropdownMenuItem(
                        value: Curves.easeOutCirc,
                        child: Text('Curves.easeOutCirc')),
                    DropdownMenuItem(
                        value: Curves.easeOutCubic,
                        child: Text('Curves.easeOutCubic')),
                    DropdownMenuItem(
                        value: Curves.easeOutExpo,
                        child: Text('Curves.easeOutExpo')),
                    DropdownMenuItem(
                        value: Curves.easeOutQuad,
                        child: Text('Curves.easeOutQuad')),
                    DropdownMenuItem(
                        value: Curves.easeOutQuart,
                        child: Text('Curves.easeOutQuart')),
                    DropdownMenuItem(
                        value: Curves.easeOutQuint,
                        child: Text('Curves.easeOutQuint')),
                    DropdownMenuItem(
                        value: Curves.fastEaseInToSlowEaseOut,
                        child: Text('Curves.fastEaseInToSlowEaseOut')),
                    DropdownMenuItem(
                        value: Curves.fastLinearToSlowEaseIn,
                        child: Text('Curves.fastLinearToSlowEaseIn')),
                    DropdownMenuItem(
                        value: Curves.fastOutSlowIn,
                        child: Text('Curves.fastOutSlowIn')),
                    DropdownMenuItem(
                        value: Curves.elasticIn,
                        child: Text('Curves.elasticIn')),
                    DropdownMenuItem(
                        value: Curves.elasticInOut,
                        child: Text('Curves.elasticInOut')),
                    DropdownMenuItem(
                        value: Curves.elasticOut,
                        child: Text('Curves.elasticOut')),
                    DropdownMenuItem(
                        value: Curves.slowMiddle,
                        child: Text('Curves.slowMiddle')),
                  ],
                  onChanged: (val) {
                    setState(() {
                      curve = val!;
                    });
                  },
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {});
                  },
                  child: Text('play'),
                ),
              ],
            ),
            const Divider(),
            Expanded(
              child: MyAnimationText(
                text:
                    'InExpended\nInExpended\nInExpended\nInExpended\nInExpended\nInExpended\nInExpended',
                curveVal: curve,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
